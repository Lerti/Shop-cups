<?
include_once 'models/Admin.php';
include_once 'models/User.php';
class AdminController extends BaseController {

    /**
     * Display moderator page
     * you can work with user's baskets
     * generate template views/admin/control.php
     */
	public function action_moderator() {
        if (isset($_SESSION['user_id_not_remember'])) {
            $session = $_SESSION['user_id_not_remember'];
        } else if (isset($_SESSION['user_id_remember'])) {
            $session = $_SESSION['user_id_remember'];
        } else {
            $session = $_SESSION['user_id'];
        }
        $role = Admin::getUserRole($session);

	    if($role == 2){
            $this->title .= '| Управление';
            $admin_object = new Admin();
            $orders = $admin_object->getOrders();
            $this->content = $this->Template('views/admin/control.php', array('products' => $orders));
	    } else {
            $this->title .= '| Главная';
            $this->content = $this->Template('views/site/master.php',
                array ('hello_info' => ''));
        }
	}

    /**
     * Display admin page
     * you can set moderator's
     * generate template views/admin/assign.php
     */
    public function action_admin() {

        if (isset($_SESSION['user_id_not_remember'])) {
            $session = $_SESSION['user_id_not_remember'];
        } else if (isset($_SESSION['user_id_remember'])) {
            $session = $_SESSION['user_id_remember'];
        } else {
            $session = $_SESSION['user_id'];
        }

        $role = Admin::getUserRole($session);
        if($role == 1){
            $this->title .= '| Управление';
            $admin_object = new Admin();
            $users = $admin_object->getUsers();
            $this->content = $this->Template('views/admin/assign.php', array('users' => $users));
        } else {
            $this->title .= '| Главная';
            $this->content = $this->Template('views/site/master.php',
                array ('hello_info' => ''));
        }
    }
}