<?

include_once ('models/User.php');

class UserController extends BaseController {

    /**
     * Display welcome-user page
     * generate template views/user/hello.php
     */
    public function action_hello() {

        $get_user = new User();
        $user_info = $get_user->getUser($_SESSION['user_id']);
        $this->title .= ' | ' . $user_info['name'];
        $this->content = $this->Template('views/user/hello.php', array('userlogin' => $user_info['login']));
    }

    /**
     * Display login|registration page
     * generate template views/user/registration.php
     */
    public function action_registration() {
        $this->title .= '| Войти / Регистрация';
		$this->library = '<script src = "js/jquery/jquery.maskedinput.min.js"></script>' .
            '<script src = "js/jquery/jquery.mask.min.js"></script>' .
            '<script src = "js/mainCheck.js"></script>' .
            '<script src = "js/Check.js"></script>';

        $this->content = $this->Template('views/user/registration.php', array('library' => $this->library));

        if ($this->isPost()) {

            if ($_POST['name']) {
                $new_user = new User();

                $result = $new_user->newUser($_POST['loginMy'], $_POST['name'],
                    $_POST['lastName'], $_POST['telephone'], $_POST['password']);
                if($result) {
                    $result = 'Вы успешно зарегистрировались <br>на сайте под логином ' . $_POST['loginMy'] . '&nbsp;!';
                    $message = $this->Template('views/site/info-text.php', array ('hello_info' => $result));
                    $this->content = $this->Template('views/user/registration.php',
                        array ('registration_message' => $message, 'library' => $this->library));
                } else {
                    $result = 'Пользователь с таким <b>логином <br></b> и (или) <b>телефоном</b> уже зарегистрирован.<br>';
                    $message = $this->Template('views/site/info-text.php', array ('hello_info' => $result));
                    $this->content = $this->Template('views/user/registration.php',
                        array ('error_registration_message' => $message, 'library' => $this->library));
                }
			} else {
				$login = new User();

				$result = $login->login($_POST['loginNew'], $_POST['passwordNew'], $_POST['checkbox_rememberMe']);
				if($result) {
                    $this->title = 'Кружки | Главная';

                    if (isset($_SESSION['user_id_not_remember'])) {
                        $user = $login->getUser($_SESSION['user_id_not_remember']);
                    } else if (isset($_SESSION['user_id_remember'])) {
                        $user = $login->getUser($_SESSION['user_id_remember']);
                    } else {
                        $user['name'] = false;
                    }

				    $result = 'Добро пожаловать на сайт, <br>' . $user['login'] . '&nbsp;!';
                    $message = $this->Template('views/site/info-text.php', array ('hello_info' => $result));
                    $this->content = $this->Template('views/site/master.php',
                        array ('hello_info' => $message));
                } else {
				    $result = 'Не удаётся войти.<br>' .
                        'Пожалуйста, проверьте правильность<br> написания <b>логина</b> и <b>пароля</b>.';
                    $message = $this->Template('views/site/info-text.php', array ('hello_info' => $result));
                    $this->content = $this->Template('views/user/registration.php',
                        array ('error_login_message' => $message, 'library' => $this->library));
                }
			}
		}
    }

    /**
     * Display logout button
     * Transition to a main page
     */
    public function action_logout() {
        $logout = new User();

        $logout->logout();

        $this->title .= '| Главная';
        $this->content = $this->Template('views/site/master.php', array ('hello_info' => ''));
    }
}