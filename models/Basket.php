<?

include_once ('DB.php');
include_once ('Admin.php');

class Basket extends DB {

	public $id_product, $id_user, $count;

    /**
     * function return user's basket
     * @param $id_user int
     * @return array
     * */
	public function getBasket($id_user) {
		if(isset($_COOKIE['guest'])) {
			setcookie('guest',"",time() - 3600 * 24 * 30);
		}
		
		return parent::getRows("SELECT `products`.`name`, `products`.`price`, `products`.`image`,
								`basket`.`count`, `basket`.`id` AS 'idB', `basket`.`id_product`, `basket`.`status`
								FROM `basket` INNER JOIN `products` 
								ON `basket`.`id_user` = '$id_user'
								AND `basket`.`id_product` = `products`.`id`
								AND `basket`.`status` = '" . Admin::NOT_ORDERED_STATUS . "'");
	}

    /**
     * function return guest's basket
     * @param $id_guest int
     * @return array
     * */
	public function getGuestBasket($id_guest) {

        if(isset($_COOKIE['guest']) === false) {
            setcookie('guest',date("l dS of F Y h:I:s A"),time() + 3600 * 24 * 30);
        }

		$row = parent::getRow("SELECT * FROM `guests` WHERE `cookie` = '$id_guest'");
			if($row) {
				return parent::getRows("SELECT `products`.`name`, `products`.`price`, `products`.`image`,
								`basket`.`count`, `basket`.`id` AS 'idB', `basket`.`id_product`
								FROM `basket` INNER JOIN `products` INNER JOIN `guests`
								ON `guests`.`cookie` = '$id_guest'
								AND `basket`.`id_guest` = `guests`.`id`
								AND `basket`.`id_product` =`products`.`id`
								AND `basket`.`status` = '" . Admin::NOT_ORDERED_STATUS . "'");
			} else {
				parent::insert("INSERT INTO `guests`(`cookie`) VALUES ('$id_guest')");
			}
	}

    /**
     * function delete item in basket
     * @param $id int
     * */
	public function deleteItem($id) {
		$row = parent::getRow("SELECT * FROM `basket` WHERE `id` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");
		$price = parent::getRow("SELECT `products`.`price` FROM `products` INNER JOIN `basket` 
		ON `basket`.`id_product` = `products`.`id` AND `basket`.`id` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");
		$count = $row['count'];
		if($count > 1) {
			$count --;
			parent::update("UPDATE `basket` SET `count` = '$count' WHERE `id` = '$id'");
			printf($count);
			printf(',');
			printf($price['price']);
		} else {
			parent::delete("DELETE FROM `basket` WHERE `id` = '$id'");
			printf('delete');
			printf(',');
			printf($price['price']);
		}
	}

    /**
     * function add item to basket
     * @param $id int
     * @param $user string|int
     * */
	public function addItem($id, $user) {
		
		if($user === 'guest') {

			$cookie = $_COOKIE['guest'];
			$row = parent::getRow("SELECT * FROM `guests` WHERE `cookie` = '$cookie'");
			if(!$row) {
				parent::insert("INSERT INTO `guests`(`cookie`) VALUES ('$cookie')");
			}
			
			$row = parent::getRow("SELECT * FROM `basket` INNER JOIN `guests` 
                                ON `guests`.`cookie`= '$cookie'
								AND `basket`.`id_guest` = `guests`.`id` 
								AND `id_product` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");
			if($row) {
				parent::update("UPDATE `basket` SET `count` = `count` + 1 
                                WHERE `id_guest` = '$row[id]' AND `id_product` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");
				printf('Не первый товар');
			} else {
				$row = parent::getRow("SELECT * FROM `guests` WHERE `guests`.`cookie` = '$cookie'");
				parent::insert("INSERT INTO `basket`(`id_product`, `id_guest`, `count`) 
								VALUES ('$id', '$row[id]', 1)");
				printf('Первый товар');
			}
		} else {
			$row = parent::getRow("SELECT * FROM `basket` WHERE `id_user` = '$user' 
                                AND  `id_product` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");

			if($row) {
				parent::update("UPDATE `basket` SET `count` = `count` + 1 
                                WHERE `id_user` = '$user' AND `id_product` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");
				printf('Не первый товар');
			} else {
				parent::insert("INSERT INTO `basket`(`id_product`, `id_user`, `count`) 
								VALUES ('$id', '$user', 1)");
				printf('Первый товар');
			}
		}
	}

    /**
     * function formalize user's basket
     * @param $id int
     * */
	public function checkItems($id) {
		parent::update("UPDATE `basket` SET `status` = '" . Admin::ORDERED_STATUS ."' 
		WHERE `id_user` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");
		printf('Ваша корзина поступила на обработку администратору сайта. Спасибо за заказ!');
	}

    /**
     * function formalize guest's basket
     * @param $name string
     * @param $lastName string
     * @param $telephone int
     * */
	public function checkGuestItems($name, $lastName, $telephone) {
		$cookie = $_COOKIE['guest'];
		$row = parent::getRow("SELECT * FROM `users` WHERE `name` = '$name' AND `lastName` = '$lastName' 
		AND `telephone` = '$telephone'");
		if(!$row) {
			parent::insert("INSERT INTO `users`(`name`, `lastName`, `telephone`) 
							VALUES ('$name', '$lastName', '$telephone')");
			$row = parent::getRow("SELECT `id` FROM `users` WHERE `name` = '$name' 
                            AND `lastName` = '$lastName' AND `telephone` = '$telephone'");
		}
		$rowTwo = parent::getRow("SELECT * FROM `guests` WHERE `cookie` = '$cookie'");
		parent::update("UPDATE `basket` SET `status` = '" . Admin::ORDERED_STATUS ."', `id_user` = '$row[id]' 
						    WHERE `id_guest` = '$rowTwo[id]' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");
		printf('Ваша корзина поступила на обработку администратору сайта. Спасибо за заказ!');
	}

	/**
     * function clean guest's basket
	*/
	public function deleteBasketGuest() {
	    $guestId = User::getGuestID();
        parent::delete("DELETE FROM `basket` 
                            WHERE `id_guest` = '$guestId' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");

	    printf('Гость. Очищено.');
    }

    /**
     * function clean user's basket
     * @param $id int
     */
    public function deleteBasketUser($id) {
        //$guestId = User::getGuestID();
        parent::delete("DELETE FROM `basket` 
                            WHERE `id_user` = '$id' AND `status` ='" . Admin::NOT_ORDERED_STATUS . "'");

        printf('Пользователь. Очищено.');
    }


}