<?
include_once 'DB.php';

class Product extends DB {

    /**
     * function get three first products from DB
     * @return array
     */
    public function getThreeProducts() {

        return parent::getRows("SELECT * FROM products WHERE id < 4");
    }

    /**
     * function get three products from DB
     * @param $MYSQL_NUMBER int
     * @return array
     */
    public function getThreeNewProducts($MYSQL_NUMBER) {

        return parent::getRows("SELECT * FROM products WHERE id >= '$MYSQL_NUMBER' LIMIT 3");
    }

    /**
     * function get product from DB
     * @param $product_id int
     * @return array
     */
    public function getProduct($product_id) {

        return parent::getRow("SELECT * FROM products where id = '$product_id'");
    }
}