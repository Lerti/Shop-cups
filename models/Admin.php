<?

include_once ('DB.php');
include_once ('User.php');

class Admin extends DB {


    const IN_WORK_STATUS = 2;
    const SEND_STATUS = 4;
    const DELETED_STATUS = 6;
    const ORDERED_STATUS = 1;
    const NOT_ORDERED_STATUS = 0;

	/**
     * function select user's orders from bd
     * @return array
	 * */
	public function getOrders() {
		return parent::getRows("SELECT `users`.`name` AS 'nameU', `users`.`lastName`, `users`.`telephone`, 
							`products`.`name`, `products`.`price`, `basket`.`count`, `basket`.`id` AS 'idB',
							`basket`.`status` 
							FROM `basket` INNER JOIN `users` INNER JOIN `products` 
							ON `basket`.`id_product` = `products`.`id` 
							AND `basket`.`id_user` = `users`.`id`
							AND `basket`.`status` != '" . self::NOT_ORDERED_STATUS . "'
							AND `basket`.`status` != '" . self::DELETED_STATUS . "'");
	}

    /**
     * function select user's information from bd
     * @return array
     * */
    public function getUsers() {
        return parent::getRows("SELECT `users`.`id`, `users`.`login`, `users`.`name`, `users`.`lastName`, `users`.`role`
							FROM `users` WHERE `users`.`role` != '" . User::ADMIN_ROLE . "' AND `users`.`login` != '0'");
    }

    /**
     * function delete user's basket (set status = 0)
     * @param $id int
     * */
	public function deleteItem($id) {
		$row = parent::getRow("SELECT * FROM `basket` WHERE `id` = '$id' ");
		$price = parent::getRow("SELECT `products`.`price` FROM `products` INNER JOIN `basket` 
		ON `basket`.`id_product` = `products`.`id` AND `basket`.`id` = '$id'");
		$count = $row[count];
		if($count > 1) {
			$count --;
			parent::update("UPDATE `basket` SET `count` = '$count' WHERE `id` = '$id'");
			printf($count);
			printf(',');
			printf($price[price]);
		} else {
			parent::update("UPDATE `basket` SET `status` = '" . self::DELETED_STATUS . "' WHERE `id` = '$id'");
			printf('delete');			
		}
	}

    /**
     * function set status 'in_work' to basket
     * @param $id int
     * */
	public function workItem($id) {
		parent::update("UPDATE `basket` SET `status` = '" . self::IN_WORK_STATUS . "' WHERE `id` = '$id'");
	}

    /**
     * function set status 'sent' to basket
     * @param $id int
     * */
	public function sendItem($id) {
		parent::update("UPDATE `basket` SET `status` = '" . self::SEND_STATUS . "' WHERE `id` = '$id'");
	}

    /**
     * function set role 'moderator' to user
     * @param $id int
     * */
    public function setModeratorRole($id) {
        parent::update("UPDATE `users` SET `role` = '" . User::MODERATOR_ROLE . "' WHERE `id` = '$id'");
    }

    /**
     * function set role 'user' to user
     * @param $id int
     * */
    public function unsetModeratorRole($id) {
        parent::update("UPDATE `users` SET `role` = '" . User::USER_ROLE . "' WHERE `id` = '$id'");
    }

    /**
     * function get user role
     * @param $user int
     * @return string
     * */
    public static function getUserRole($user) {
        $get_user = new User();
        if (isset($user)) {
            $user_info = $get_user->getUser($user);
            $role = $user_info['role'];
        } else {
            $role = 'guest';
        }
        return $role;
    }
}
