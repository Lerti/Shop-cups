<?

include_once ('DB.php');
include_once ('Admin.php');

class User extends DB {

    const ADMIN_ROLE = 1;
    const MODERATOR_ROLE = 2;
    const USER_ROLE = 0;

    /**
     * function get user from DB
     * @param $id int
     * @return array
     */
    public function getUser($id) {

        return parent::getRow("SELECT * FROM `users` WHERE  `id` = '$id'");
    }

    /**
     * function get user from DB
     * @param $cookie string
     * @return array
     */
    public function getUserByCookie($cookie) {

        return parent::getRow("SELECT * FROM `users` WHERE  `cookie` = '$cookie'");
    }

    /**
     * function insert user to DB
     * @param $login string
     * @param $name string
     * @param $lastName string
     * @param $telephone int
     * @param $password string
     * @return string
     */
    public function newUser($login, $name, $lastName, $telephone, $password) {

        $cookie = date("l dS of F Y h:I:s A");
        $object = [
            'login' => strip_tags($login),
            'name' => strip_tags($name),
            'lastName' => strip_tags($lastName),
            'telephone' => strip_tags($telephone),
            'password' => parent::Password(strip_tags($login), strip_tags($password)),
            'cookie' => $cookie,
        ];

        $user = parent::getRow("SELECT * FROM `users` WHERE  `login` = '$login'");
        $user2 = parent::getRow("SELECT * FROM `users` WHERE  `telephone` = '$telephone'");
		
        if (!$user && !$user2) {
            $columns = array();
            $masks = array();

            foreach ($object as $key => $value) {

                $columns[] = $key;
                $masks[] = ":$key";
                if ($value === null) {
                    $object[$key] = 'NULL';
                }
            }
            $columns_s = implode(',', $columns);
            $masks_s = implode(',', $masks);

            parent::insert("INSERT INTO `users` ($columns_s) VALUES ($masks_s)", $object);
            return true;
        } else {
            return false;
        }
    }

    /**
     * function login user
     * @param $login string
     * @param $password string
     * @param $cookie string
     * @return string
     */
    public function login($login, $password, $cookie) {

        $loginUser =  strip_tags($login);
        $user = parent::getRow("SELECT * FROM `users` WHERE  `login` = '$loginUser'");

        if ($user) {
            $DB = strip_tags($password);
            $passwordDB = parent::Password($user['login'],$DB);
            if ($user['password'] == $passwordDB) {

                if(isset($_COOKIE['guest'])) {
                    setcookie('guest','',time() - 3600 * 24 * 30);
                }

                if($_POST['checkbox_rememberMe']) {
                    $cookie = $user['cookie'];
                    if(isset($_COOKIE['user']) === false || $_COOKIE['user'] !== $cookie) {
                        setcookie('user',$cookie,time() + 3600 * 24 * 30);
                    }
                    $_SESSION['user_id_remember'] = $user['id'];
                } else {
                    $_SESSION['user_id_not_remember'] = $user['id'];
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * function logout user
     * @return bool
     */
    public function logout() {

        if (isset($_SESSION["user_id_remember"])) {
            if(isset($_COOKIE['user'])) {
                setcookie('user','',time() - 3600 * 24 * 30);
            }
            unset($_SESSION["user_id_remember"]);
            session_destroy();
            return true;
        } else if(isset($_SESSION["user_id_not_remember"])) {
            unset($_SESSION["user_id_not_remember"]);
            session_destroy();
            return true;
        } else {
            return false;
        }
    }

    /**
     * function get count of user's orders
     * @param $id int
     * @return int
    */
    public static function getCount($id) {
        $count = 0;
        $orders = parent::getRows("SELECT `count` FROM `basket` 
                                  WHERE `id_user` = '$id' AND `status` = '" . Admin::NOT_ORDERED_STATUS . "'");
        foreach($orders as $order) {
            $count += $order['count'];
        }
        return $count;
    }

    /**
     * function get count of guest's orders
     * @return int
     */
    public static function getGuestCount() {
        $cookie = $_COOKIE['guest'];
        $count = 0;
        $id = parent::getRow("SELECT `id` FROM `guests` WHERE `cookie` = '$cookie'");
        if(!$id) {
            return $count;
        }
        $orders = parent::getRows("SELECT `count` FROM `basket` 
                                  WHERE `id_guest` = '$id[id]' AND `status` = '" . Admin::NOT_ORDERED_STATUS . "'");
        foreach($orders as $order) {
            $count += $order['count'];
        }
        return $count;
    }

    /**
     * function get guest's id
     * @return int
     */
    public static function getGuestID() {
        $cookie = $_COOKIE['guest'];
        $id = parent::getRow("SELECT `id` FROM `guests` WHERE `cookie` = '$cookie'");

        return $id['id'];
    }

    public static function getUserId() {
        $user_id = 'guest';
        if (isset($_SESSION['user_id_not_remember'])) {
            $user_id = $_SESSION['user_id_not_remember'];
        } else if (isset($_SESSION['user_id_remember']) || isset($_COOKIE['user'])) {
            $user_id = $_SESSION['user_id_remember'];
        }
        return $user_id;
    }
}
