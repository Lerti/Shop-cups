let number = 4;


$(document).ready(() => {

    //to load data on the catalog.php page when you click "Show more"
    $("#yet").click(() => {
        $.ajax({
            url: "views/site/item.php",
            type: "GET",
            data: {"number": number},
            error: function () {
                console.log("Что-то пошло не так в передаче при подгрузке товаров...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Больше нет записей");
                } else {
                    $("#products").append(response);
                    number += 3;

                    $.each($(".all-items"),function(){
                        if(!($(this).is(':empty'))) {
                            $('#yet').addClass('hide');
                        }
                    });
                }
            }
        });


    });
	
	//delete item from the basket in moderator profile
	$(".delete_admin").click( e => {
		let del_id = e.currentTarget["id"];
        $.ajax({
            url: "actions/moderatorAction.php",
            type: "GET",
            data: {"delete_id": del_id},
            error: function () {
                console.log("Что-то пошло не так в передаче при удалении товара...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при удалении товара...");
                } else {
					let string = response.split(',');
					if(string[0] === 'delete') {
						let new_delete = "product_" + del_id;
						$("#" + new_delete).empty();
					} else {	
						let new_count = "count_" + del_id;
						let new_price = "price_" + del_id;

						$("#" + new_count).text(string[0]);
						$("#" + new_price).text(string[0] * string[1]);
					}   
                }
            }
        });
    });

    //formalize user's basket in moderator profile
	$(".work_admin").click( e => {
		let work_id = e.currentTarget["id"];
        $.ajax({
            url: "actions/moderatorAction.php",
            type: "GET",
            data: {"work_id": work_id},
            error: function () {
                console.log("Что-то пошло не так в передаче при обработке товара (work)...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при обработке товара (work)...");
                } else {
					let new_work = "work_" + work_id;
					$("#" + new_work).text("Оформлен");
                }
            }
        });
    });

    //send user's basket in moderator profile
	$(".sent_admin").click( e => {
		let sent_id = e.currentTarget["id"];
        $.ajax({
            url: "actions/moderatorAction.php",
            type: "GET",
            data: {"sent_id": sent_id},
            error: function () {
                console.log("Что-то пошло не так в передаче при обработке товара (sent)...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при обработке товара (sent)...");
                } else {
					let new_sent = "sent_" + sent_id;
					$("#" + new_sent).text("Отправлен");

					let work = document.getElementById("work_" + sent_id);
                    $(work).text("Оформлен");
                }
            }
        });
    });

    //add status moderator to user in admin profile
    $(".assign_admin").click( e => {
        let assign_id = e.currentTarget["id"];
        $.ajax({
            url: "actions/adminAction.php",
            type: "GET",
            data: {"assign_id": assign_id},
            error: function () {
                console.log("Что-то пошло не так в передаче при добавлении статуса модератора пользователю...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при добавлении статуса модератора пользователю...");
                } else {
                    let new_moderator = "role_" + assign_id;

                    $("#" + new_moderator).text("модератор");
                    $(e.currentTarget).addClass('hide');
                    $($("#" + "assign_debar_" + assign_id)[0].children[0]).removeClass('hide');
                }
            }
        });
    });

    //delete status moderator to user in admin profile
    $(".debar_admin").click( e => {
        let debar_id = e.currentTarget["id"];
        $.ajax({
            url: "actions/adminAction.php",
            type: "GET",
            data: {"debar_id": debar_id},
            error: function () {
                console.log("Что-то пошло не так в передаче при удалении статуса модератора у пользователя...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при удалении статуса модератора у пользователя...");
                } else {
                    let new_user = "role_" + debar_id;

                    $("#" + new_user).text("-");
                    $(e.currentTarget).addClass('hide');
                    $($("#" + "assign_debar_" + debar_id)[0].children[1]).removeClass('hide');
                }
            }
        });
    });

    //delete item from the basket in user or guest profile
	$(".delete_basket").click( e => {
		let del_id = e.currentTarget["id"];
		let total_id = $("#total");
		let string_total = total_id.text().split(' ');
        $.ajax({
            url: "actions/userAction.php",
            type: "GET",
            data: {"delete_id": del_id},
            error: function () {
                console.log("Что-то пошло не так в передаче при удалении товара из корзины...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при удалении товара из корзины...");
                } else {
					let string = response.split(',');
					if(string[0] === 'delete') {
						let new_delete = "productU_" + del_id;
						$("#" + new_delete).remove();
						
						$("#total").empty();
						string_total[1] -= string[1];
						$("#total").append(string_total[0], ' ', string_total[1], ' ',string_total[2]);

						if($('#basket-table tbody tr').length === 1) {
                            $(".table").addClass('hideImportant');
                            $("#total").addClass('hideImportant');
                            $(".delete_all_basket").addClass('hideImportant');
                            $("#guestOrder").addClass('hideImportant');
                            $(".add_basket").addClass('hideImportant');

                            $(".empty-cart").addClass('show');
                        }
					} else {
						let new_count = "countU_" + del_id;
						let new_price = "priceU_" + del_id;
						
						$("#total").empty();
						string_total[1] -= string[1];
						$("#total").append(string_total[0], ' ', string_total[1], ' ', string_total[2]);

						$("#" + new_count).text(string[0]);
						$("#" + new_price).text(string[0] * string[1]);
					}
                }
                if($("#count").length) {
                    let $count = Number($("#count").text());
                    $("#count").text($count - 1);
                } else {
                    let $guestCount = Number($("#count-guest").text());
                    $("#count-guest").text($guestCount - 1);
                }
            }
        });
    });

    //delete all items from the basket in user or guest profile
    $(".delete_all_basket").click( e => {
        let del_id = e.currentTarget["id"];
        let string_total = del_id.split("_");
        console.log(string_total);
        $.ajax({
            url: "actions/userAction.php",
            type: "GET",
            data: {"delete_all_role": string_total[1],
                "user_id": string_total[2]},
            error: function () {
                console.log("Что-то пошло не так в передаче при очистке корзины...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при очистке корзины...");
                } else {
                    if(response === 'Гость. Очищено.') {
                        $("#guestOrder").addClass('hideImportant');
                        $("#count-guest").text('0');
                    } else if(response === 'Пользователь. Очищено.') {
                        $("#count").text('0');
                        $(".add_basket").addClass('hideImportant');
                    }
                    $(".table").addClass('hideImportant');
                    $("#total").addClass('hideImportant');
                    $(".delete_all_basket").addClass('hideImportant');
                    $(".empty-cart").addClass('show');
                }
            }
        });
    });

    //add item to the basket in user profile
	$("#content").on('click', '.add-to-basket', e => {
		e.preventDefault();
		let add_id = e.currentTarget["id"];
		let string = add_id.split('_');
		$.ajax({
            url: "actions/userAction.php",
            type: "GET",
            data: {"add": string[0],
					"addIdP": string[1],
					"addIdU": string[2]},
            error: function () {
                console.log("Что-то пошло не так в передаче при добавлении товара в корзину...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при добавлении товара в корзину...");
                } else {
                    let message;
                    let div = "#" + "divMessage_" + string[1];
                    let p = "#" + "textMessage_" + string[1];
                    if (response === 'Первый товар' && string[0] === 'add') {
                        message = 'Кружка добавлена в Вашу корзину!';
                        $(div).addClass('small-message-one');
                    } else if (response === 'Первый товар' && string[0] === 'add-big') {
                        message = 'Кружка добавлена в Вашу корзину!';
                        $(div).addClass('big-message-one');
                    } else if (response === 'Не первый товар' && string[0] === 'add') {
                        message = 'Еще одна кружка добавлена в Вашу корзину!';
                        $(div).removeClass('small-message-one');
                        $(div).addClass('small-message-two');
                    } else if (response === 'Не первый товар' && string[0] === 'add-big') {
                        message = 'Еще одна кружка добавлена в Вашу корзину!';
                        $(div).removeClass('big-message-one');
                        $(div).addClass('big-message-two');
                    }

                    $(div).removeClass('hide');
                    $(p).text(message);

                    if($("#count").length) {
                        let $count = Number($("#count").text());
                        $("#count").text($count + 1);
                    } else {
                        let $guestCount = Number($("#count-guest").text());
                        $("#count-guest").text($guestCount + 1);
                    }
                }
            }
        });
	});

    //formalize user's basket
	$(".add_basket").click( e => {
		let check_id = e.currentTarget["id"];
		let string = check_id.split('_');
        $.ajax({
            url: "actions/userAction.php",
            type: "GET",
            data: {"check_id": string[1]},
            error: function () {
                console.log("Что-то пошло не так в передаче при оформлении корзины...");
            },
            success: function (response) {
                if (response === 0) {
                    console.log("Что-то пошло не так при оформлении корзины...");
                } else {
                    $("#" + check_id).addClass('hide');
                    $("#basket_user_" + string[1]).addClass('hideImportant');
                    $("#count").text('0');
                    $('#content').append('<p class = info-formalize-basket>' + response + '</p>');
                }
            }
        });
    });

    //formalize guest's basket
	$(".add_new_basket").click( e => {
		e.preventDefault();
		let check_id = e.currentTarget.form;
		let name = check_id[0].value;
		let lastName = check_id[1].value;
		let telephone = check_id[2].value;
		if(name !== '' && lastName !== '' && telephone !== ''){
			
			$.ajax({
				url: "actions/userAction.php",
				type: "GET",
				data: {"guest_name": name,
				"guest_lastName": lastName,
				"guest_telephone": telephone},
				error: function () {
                    console.log("Что-то пошло не так в передаче при оформлении корзины гостя...");
				},
				success: function (response) {
					if (response === 0) {
                        console.log("Что-то пошло не так при оформлении корзины гостя...");
					} else {
					$(check_id).text(response);
					$("#basket_guest").addClass('hideImportant');
                    $("#count-guest").text('0');
					}
				}
			});
		}
    });

    //to highlight the active menu item
    let url = document.location.href;
	$.each($("#menu a"),function(){
		if(this.href == url){
			$(this).addClass('active');
		}
	});

	$(".reg-what").click(e => {
		let name = e.currentTarget;
		if($(name).hasClass('not-active-reg')) {
			$.each($(".reg-what"),function(){
				$(this).removeClass('active-reg');
				$(this).addClass('not-active-reg');
			});
			$(name).removeClass('not-active-reg');
            $(name).addClass('active-reg');
			
			if($(name).is("#login")) {
				$("#log").removeClass('unseen');
				$("#registration").addClass('unseen');
			} else {
				$("#registration").removeClass('unseen');
				$("#log").addClass('unseen');
			}
		}
	});
});

