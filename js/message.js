$(document).ready(() => {

    //add message to user of adding product from catalog-page and product-page to basket
    $("#content").on('click', '.info-item', e => {
        $(e.currentTarget).addClass('hide');
    });

    $("#content").on('click', '.big-message-one', e => {
        $(e.currentTarget).removeClass('big-message-one');
        $(e.currentTarget).addClass('hide');
    });

    $("#content").on('click', '.big-message-two', e => {
        $(e.currentTarget).removeClass('big-message-two');
        $(e.currentTarget).addClass('hide');
    });
});
