$(document).ready(() => {
	let my;

	//for telephone mask with jquery
	$("#telephone").mask("+7 (999) 99-99-999", { autoclear: false, placeholder:" " });
	$("#guestTelephone").mask("+7 (999) 99-99-999", { autoclear: false, placeholder:" " });

	$("#loginMy").on("keyup", function() { //when login change
		my = new Check (document.getElementsByTagName('input')['loginMy']);
	});
	
	$("#name").on("keyup", function() { //when name change
        my = new Check (document.getElementsByTagName('input')['name']);
	});

	$("#lastName").on("keyup", function() { //when last name change
        my = new Check (document.getElementsByTagName('input')['lastName']);
	});

    $("#telephone").on("keyup", function() { //when login change
        my = new Check (document.getElementsByTagName('input')['telephone']);
    });

	$("#password").on("keyup", function() { //when first password field change
        my = new Check (document.getElementsByTagName('input')['password']);
	});

	$("#passwordRepeat").on("keyup", function() { //when second password field change
		let value1 = $("#password").val(); 
		let value2 = $(this).val();
        my = new Check (" ");
        my._checkPasswords(value1, value2)
	});

});