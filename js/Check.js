class Check {
    //information to validate users's contact information
    constructor(field) {
        this.patterns = {
            loginMy: /^([a-zA-Z]|[\u0400-\u04FF]){4,15}$/,
            name: /^[\u0400-\u04FF]{4,15}$/,
            lastName: /^[\u0400-\u04FF]{4,20}$/,
            //telephone: / ^ \+\7 \s{1}\(\d{3}\)\s{1}\d{2}\-\d{2}\-\d{3} $/,
            password: /^\w{4,15}$/
        };
        this.errors = {
            loginMy: 'из 4-15 букв',
            name: 'из 4-15 русских букв',
            lastName: 'из 4-20 русских букв',
            //telephone: '+7 (999) 99-99-999',
            password: 'из 4-15 латинских букв и цифр'
        };

        this.errorClass = 'error-msg';
        this.field = field;
        this._check(this.field);
    }

    //validate users's contact information
    _check(field) {

        if (this.patterns[field.name]) {
            if (!this.patterns[field.name].test(field.value)) {
                if ($(field.parentNode.lastChild).hasClass(this.errorClass) === false) {
                    let errMsg = document.createElement('div');
                    errMsg.classList.add(this.errorClass);
                    errMsg.textContent = this.errors[field.name];
                    field.parentNode.appendChild(errMsg);
                    $("#submitReg").attr("disabled", "disabled");
                }
            } else {
                if (field.parentNode.lastChild !== field) {
                    field.parentNode.lastChild.remove();
                }
                let login = document.getElementsByTagName('input')['loginMy'];
                let name = document.getElementsByTagName('input')['name'];
                let lastName = document.getElementsByTagName('input')['lastName'];
                let password = document.getElementsByTagName('input')['password'];
                let passwordNew = document.getElementsByTagName('input')['passwordRepeat'];

                if(this.patterns[login.name].test(login.value)
                    && this.patterns[name.name].test(name.value)
                    && this.patterns[lastName.name].test(lastName.value)
                    && this.patterns[password.name].test(password.value)
                    && this.patterns[password.name].test(passwordNew.value)) {

                    $("#submitReg").removeAttr("disabled");
                    if (field.parentNode.lastChild !== field) {
                        field.parentNode.lastChild.remove();
                    }
                }
            }
        }

    }

    //validate users's password
    _checkPasswords(one,two) {
        if(one !== two) {
            $("#errorPassRepeat").html("Пароли не совпадают!");
            $("#submitReg").attr("disabled", "disabled");
        } else {
            let login = document.getElementsByTagName('input')['loginMy'];
            let name = document.getElementsByTagName('input')['name'];
            let lastName = document.getElementsByTagName('input')['lastName'];
            let password = document.getElementsByTagName('input')['password'];
            let passwordNew = document.getElementsByTagName('input')['passwordRepeat'];

            if(this.patterns[login.name].test(login.value)
                && this.patterns[name.name].test(name.value)
                && this.patterns[lastName.name].test(lastName.value)
                && this.patterns[password.name].test(password.value)
                && this.patterns[password.name].test(passwordNew.value)) {
                    $("#submitReg").removeAttr("disabled");
                }
            $("#errorPassRepeat").html("");
        }
    }
}