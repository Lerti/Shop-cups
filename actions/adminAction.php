<?
/**
 * Treatment admin actions
 * add status delete, in_work, sent to basket
*/
require_once('../models/Admin.php');

if (isset($_GET['assign_id'])) {
		$id = $_GET['assign_id'];
		$admin_object = new Admin();
		$admin_object->setModeratorRole($id);
}

if (isset($_GET['debar_id'])) {
    $id = $_GET['debar_id'];
    $admin_object = new Admin();
    $admin_object->unsetModeratorRole($id);
}



