<?
/**
 * Treatment user actions
 * delete product from basket
 * add product to basket
 * check basket
 * check user data (if user is guest)
 */
require_once('../models/Basket.php');

if (isset($_GET['delete_id'])) {
	$id = $_GET['delete_id'];
	$admin_object = new Basket();
	$admin_object->deleteItem($id);
}

if (isset($_GET['add'])) {	
	$id = $_GET['addIdP'];
	$user = $_GET['addIdU'];
	$admin_object = new Basket();
	$admin_object->addItem($id, $user);
}

if (isset($_GET['check_id'])) {
	$id = $_GET['check_id'];
	$admin_object = new Basket();
	$admin_object->checkItems($id);
}

if (isset($_GET['guest_name'])) {
	$name = $_GET['guest_name'];
	$lastName = $_GET['guest_lastName'];
	$telephone = $_GET['guest_telephone'];
	$admin_object = new Basket();
	$admin_object->checkGuestItems($name, $lastName, $telephone);
}

if (isset($_GET['delete_all_role'])) {
    $role = $_GET['delete_all_role'];
    $admin_object = new Basket();

    if($role == 'guest') {
        $admin_object->deleteBasketGuest();
    } else if ($role == 'user') {
        $id= $_GET['user_id'];
        $admin_object->deleteBasketUser($id);
    } else {
        printf('Error!!!');
    }
}