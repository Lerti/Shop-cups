<?
/**
 * view of basket page
 * @var $products array
 * @var $information_guest string guest.php
*/
?>
<?if (User::getGuestCount() !== 0 || User::getCount($user_id) !== 0) {  ?>
    <div class = 'empty-cart hide'>
        <img src = '<?= DIR_MAIN . 'empty-cart.png' ?>' alt = 'Пустая корзина'>
        <div>
            <h2>Ваша Корзина пуста</h2>
            <p>Но это легко поправить! Отправляйтесь за покупками
                или авторизуйтесь, чтобы увидеть уже добавленные товары.</p>
        </div>
    </div>

    <table class = "table" id = "basket-table">
	    <tr>
		    <th>Что</th>
		    <th>Изображение</th>
		    <th>Сколько</th>
		    <th>Почём</th>
		    <th>Итого (руб)</th>
		    <th>Убрать</th>
	    </tr>
	    <?
		    $order = 0;
		    if (isset($products)) {
			    foreach ($products as $product) {
				    echo "<tr id = 'productU_" . $product["idB"] . "'>
				    <td>" . $product["name"] . "</td>
				    <td> <img src = " . DIR_SMALL . $product["image"] . " alt = " . $product["name"] . " </td>
				    <td id = 'countU_" . $product["idB"] ."'>" . $product["count"] . "</td>
				    <td>" . $product["price"] . "</td>
				    <td id = 'priceU_" . $product["idB"] . "'>" . $product["count"] * $product["price"] . "</td>
				    <td>
					    <button class = 'button delete_basket' id = '" . $product["idB"] . "' type = 'submit'>Удалить</button></td>
				    </td>
				    </tr>";
				    $order += $product["count"] * $product["price"];
			    }
		    }
		    if(!$products){
			    echo "<tr>
				    <td>" . '-' . "</td>
				    <td>" . '-' . "</td>
				    <td>" . '-' . "</td>
				    <td>" . '-' . "</td>
				    <td>" . '-' . "</td>
				    <td>" . '-' . "</td>
				    </tr>";
		    }
	    ?>
    </table>
    <br>

    <h2 class = "totat_h2" id = "total"><?= "Итого: " . $order . " рублей"?></h2>
    <br>
<? } else {?>
    <div class = 'empty-cart'>
        <img src = '<?= DIR_MAIN . 'empty-cart.png' ?>' alt = 'Пустая корзина'>
        <div>
            <h2>Ваша Корзина пуста</h2>
            <p>Но это легко поправить! Отправляйтесь за покупками
                или авторизуйтесь, чтобы увидеть уже добавленные товары.</p>
        </div>
    </div>
<? } ?>
<?=$information_guest?>

<?if ($user_id != 'guest' && User::getCount($user_id) !== 0) {  ?>
	<button id = "basket_<?=$user_id?>" class = "button add_basket" type = "submit">Оформить заказ</button>
    <button id = "basket_user_<?=$user_id?>" class = "button delete_all_basket" type = "submit">Очистить корзину</button>
<?}