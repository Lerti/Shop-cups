<?php
/**
 * view of products on catalog page
 *
*/

session_start();

/**
 * check a user's status (user|guest)
 */
$user_id = 'guest';
if (isset($_SESSION['user_id_not_remember'])) {
    $user_id = $_SESSION['user_id_not_remember'];
} else if (isset($_SESSION['user_id_remember'])) {
    $user_id = $_SESSION['user_id_remember'];
}

if (isset($_GET['number'])) {

	require_once "../../models/DB.php";
	require_once "../../models/Product.php";
    $MYSQL_NUMBER = $_GET['number'];
    $product_object = new Product();
	
	$products = $product_object->getThreeNewProducts($MYSQL_NUMBER);
}

if ($products) {
	if($MYSQL_NUMBER){
		$MYSQL_NUMBER += 3;
	} else { 
		?> <div id = 'products'> <?
	} ?>
		
	<? foreach ($products as $product) { ?>
		<div class = "item">
			<a href = "index.php?c=site&act=product&id=<?= $product['id'] ?>">
				<h3 class = "item-name"><?= $product['name'] ?></h3>
				<img class = "small-image" src = "<?= DIR_SMALL . $product['image'] ?>" alt = '<?= $product['name'] ?>'>
			</a>
            <div class = 'info-item hide' id = 'divMessage_<?= $product['id'] ?>'>
                <p class = 'info-text' id = 'textMessage_<?= $product['id'] ?>'></p>
            </div>

			<p class = "price"><?= $product['price'] ?>р.</p>
			<a href = "#" id = "add_<?=$product['id']?>_<?=$user_id?>"
               title = "Добавить в корзину" class = "add-to-basket">Купить</a>

		</div>
	<? }
} else { ?>
    <div class = 'all-items'>
       <p>Вы просмотрели все товары.</p>
    </div>
<? }
if(!$MYSQL_NUMBER) {
	?> </div>
<? }
