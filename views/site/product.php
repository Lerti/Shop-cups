<?php
/**
 * view of one big item
 * @var $product array
*/
?>
<div class = "product">
    <h2><?= $product['name'] ?></h2>
    <img class = "big-image" src = "<?= DIR_BIG.$product['image'] ?>"
         alt = "<?= $product['name'] ?>" title = "<?= $product['name'] ?>" >
    <div class = "big-item-information">
		<h3>Объем:</h3>
		<p><?= $product['volume'] ?> миллитров</p>
		<h3>Описание товара:</h3>
		<p class = "text"><?= $product['description'] ?></p>
		<h3>Цена:</h3>
		<p><?= $product['price'] ?> р. <br/><br/></p>
		<a href = "#" id = "add-big_<?=$product['id']?>_<?=$user_id?>"
           title = "Добавить в корзину" class = "add-to-basket">Купить</a>
    </div>
    <div class = 'info-item hide' id = 'divMessage_<?= $product['id'] ?>'>
        <p class = 'info-text' id = 'textMessage_<?= $product['id'] ?>'></p>
    </div>
</div>