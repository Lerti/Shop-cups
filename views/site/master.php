<?
/**
 * view of main page
 * @var $hello_info string information message info-text.php
*/
?>
<div id = "login-message">
    <?=$hello_info?>
</div>
<div id = "carousel-comics" class = "carousel slide" data-ride = "carousel" style = "display: inline-block;">
    <ol class="carousel-indicators">
        <li data-target = "#carousel-comics" data-slide-to = "0" class = "active"></li>
        <li data-target = "#carousel-comics" data-slide-to = "1"></li>
        <li data-target = "#carousel-comics" data-slide-to = "2"></li>
        <li data-target = "#carousel-comics" data-slide-to = "3"></li>
        <li data-target = "#carousel-comics" data-slide-to = "4"></li>
    </ol>
    <div class = "carousel-inner">
        <div class = "carousel-item active">
            <img src = "<?= DIR_MAIN . 'carousel1.jpg' ?>" alt = "Анчан">
        </div>
        <div class = "carousel-item">
            <img src = "<?= DIR_MAIN . 'carousel2.jpg' ?>" alt = "Пуэр">
        </div>
        <div class = "carousel-item">
            <img src = "<?= DIR_MAIN . 'carousel3.jpg' ?>" alt = "Матэ">
        </div>
        <div class = "carousel-item">
            <img src = "<?= DIR_MAIN . 'carousel4.jpg' ?>" alt = "Кудин">
        </div>
        <div class = "carousel-item">
            <img src = "<?= DIR_MAIN . 'carousel5.jpg' ?>" alt = "Шу пуэр">
        </div>
    </div>
    <a class = "carousel-control-prev" href = "#carousel-comics" role = "button" data-slide = "prev">
        <span class = "fa fa-chevron-left" aria-hidden = "true"></span>
        <span class = "sr-only">Предыдущий</span>
    </a>
    <a class = "carousel-control-next" href = "#carousel-comics" role = "button" data-slide = "next">
        <span class = "fa fa-chevron-right" aria-hidden = "true"></span>
        <span class = "sr-only">Следующий</span>
    </a>
</div>


