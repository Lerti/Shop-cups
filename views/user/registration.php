<?
/**
 * view of registration page
 * @var $error_registration_message string
 * @var $registration_message string
 * @var $error_login_message string
 */
?>

<div class = "reg">
	<div id = "reg-name">
        <a href = "#" class = "active-reg reg-what" id = "login">Войти<a> / <a href = "#" class = "not-active-reg reg-what">Регистрация</a>
    </div>
    <div id = "error-registration-message">
        <?= $error_registration_message ?>
    </div>
    <div id = "registration-message">
        <?= $registration_message ?>
    </div>
    <div id = "error-login-message">
        <?= $error_login_message ?>
    </div>
	<div id = "log">
	<form method = "post">
        <label for = "loginNew">Логин&nbsp;</label>
        <input class = "input-data" type = "text" name = "loginNew" id = "loginNew"/><br>
        <label for = "passwordNew">Пароль</label>
        <input class = "input-data" type = "password" name = "passwordNew" id = "passwordNew"/><br>
        <input class = "checkbox" type = "checkbox"  id = "rememberMe" name = "checkbox_rememberMe"/>
        <label for = "rememberMe">Запомнить?</label>
			<p><input class = "button"  type = "submit" value = "Войти" /></p>
		</form>
	</div>
	
	<div class = "unseen" id = "registration">
		<form method = "post" id = "myForm">
		
			<div class = "field">
				<label for = "loginMy">Логин</label>
				<input type = "text" class = "input-data" name = "loginMy" id = "loginMy" required />
			</div>

			<div class = "field">
				<label for = "name">Имя</label>
				<input type = "text" class = "input-data" name = "name" id = "name" required />
			</div>

			<div class = "field">
				<label for = "lastName">Фамилия</label>
				<input type = "text" class = "input-data" name = "lastName" id = "lastName" required />
			</div>

			<div class = "field">
				<label for = "telephone">Телефон </label>
				<input type = "text" class = "input-data" name = "telephone" id = "telephone" placeholder = "+7 (999) 99 99 999" required />
			</div>
			<div class = "field">
				<label for = "password">Пароль</label>
				<input type = "password" class = "input-data" name = "password" id = "password" required />
			</div>

			<div class = "field">
				<label for = "passwordRepeat">Подтвердите пароль</label>
				<br/>
				<input type = "password" class = "input-data" name = "passwordRepeat" id = "passwordRepeat" required />
			</div>
            <div id = "errorPassRepeat"></div>
			<p><input class = "button" type = "submit" id = "submitReg" value = "Зарегистрироваться" /></p>
		</form>
	</div>	
</div>



