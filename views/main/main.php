<?
/**
 * view of main page
 * @var $title string
 * @var $user User
 * @var $content array
 * @var $date string
 * @var $library string
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns = "http://www.w3.org/1999/html">
<head>
	<title><?=$title?></title>
	<meta content = "text/html; charset =UTF-81" http-equiv = "content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href = "https://fonts.googleapis.com/css?family=Caveat:400,700&amp;subset=cyrillic" rel = "stylesheet">
    <link rel = "stylesheet" type = "text/css" media = "screen" href = "css/sstyle.css" />
    <link rel = "stylesheet" type = "text/css" media = "screen" href = "css/font-awesome/css/font-awesome.min.css">
    <link rel = "stylesheet" type = "text/css" media = "screen" href = "css/bootstrap-carousel.css" />
    <link rel="shortcut icon" href="/images/main/favicon.jpg" type="image/jpg">
</head>
<body>
	<div id = "header">
        <a href = "index.php" class = "logo">
            <img src = "../../images/main/logo.png" alt = "логотип интернет-магазина 'Кружки для всех'">
        </a>
        <div class = "headerText">
            <h1>
                Интернет-магазин "Кружки для всех"
                <br>
            </h1>
            <h2>
                Устройте своё Безумное чаепитие!
                <br><br><br><br>
            </h2>
        </div>
	</div>
    </div>
	<nav id = "menu">
        <ul>
            <li><a href = "index.php" id = 'main-page'>Главная  </a></li>
            <?
            if ( $user['role'] != User::USER_ROLE) {
                if($user['role'] == User::MODERATOR_ROLE) {
                    echo '<li><a href = "index.php?c=admin&act=moderator">Управление</a></li>';
                } else if ($user['role'] == User::ADMIN_ROLE) {
                    echo '<li><a href = "index.php?c=admin&act=admin">Управление</a></li>';
                }
            } else {
                echo '<li><a href = "index.php?c=site&act=catalog">Каталог </a></li>';
            }
            if ($user['name']) {
                if($user['role'] == User::USER_ROLE) {
                    echo '<li><a href = "index.php?c=site&act=basket">Корзина ('.
                        '<p id = "count">' . User::getCount($user['id']) . '</p>)</a></li>';
                }
                    echo '<li><a href="index.php?c=user&act=logout">Выйти (' . $user['login'] . ')</a></li>';
            } else {
                echo '<li><a href = "index.php?c=site&act=basket">Корзина('.
                    '<p id = "count-guest">' . User::getGuestCount() . '</p>)</a></li>' .
                    '<li><a href = "index.php?c=user&act=registration" id = "reg-page">Войти&nbsp;/&nbsp;Регистрация </a></li>';
            }
        ?>

        </ul>
	</nav>
	
	<div id = "content">
        <?=$content?>
	</div>

	<div id = "footer">
        &#169; Все права защищены. Адрес. Телефон. <?=$date . " г." ?>
	</div>
	
    <script src = "js/jquery/jquery-3.0.0.min.js"></script>
	
    <script src = "js/button.js"></script>
    <script src = "js/message.js"></script>
    <script src = "js/bootstrap/bootstrap.min.js"></script>
    <script src = "js/carousel.js"></script>
	<?=$library ?>
</body>
</html>
