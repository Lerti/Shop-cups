<?
/**
 * view of control admin page
 * @var $users array
 */
?>
<?
if (!empty($users)) {
?>

    <table class = "table" id = "admin-table" >
	    <tr>
            <th>Логин</th>
            <th>Имя</th>
		    <th>Фамилия</th>
		    <th>Роль</th>
		    <th>Назначение | Отстранение</th>
	    </tr>
	    <?
		    foreach ($users as $user) {
			    echo "<td>" . $user["login"] . "</td>
			    <td>" . $user["name"] . "</td>
			    <td>" . $user["lastName"] . "</td>
			    <td id = 'role_" . $user['id'] . "'>";
			    if($user["role"] == User::MODERATOR_ROLE) {
				    echo "модератор";
			    } else {
				    echo "-";
			    }
			    echo "</td>
                <td id = 'assign_debar_" . $user['id'] . "'>";
			    if($user["role"] == User::MODERATOR_ROLE) {
                    echo "<button class = 'button debar_admin' id = '".$user["id"] . "'  type = 'submit'>Отстранить</button>" .
                        "<button class = 'button assign_admin hide' id = '".$user["id"] . "'  type = 'submit'>Назначить</button>";
			    } else {
				    echo "<button class = 'button debar_admin hide' id = '".$user["id"] . "'  type = 'submit'>Отстранить</button>" .
				    "<button class = 'button assign_admin' id = '".$user["id"] . "'  type = 'submit'>Назначить</button>";
			    }
			    echo "</td>
			    </tr>";
		    }
	    ?>
    </table>
    <br>
<? } else {?>
    <p id = "info-admin-not-users-message">Пользователей нет ...  </p>
<?}?>

